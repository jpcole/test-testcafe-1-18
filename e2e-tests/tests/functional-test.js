import {Selector} from 'testcafe';


fixture `sample test`

test('look at home page', async t => {
    await t.navigateTo('http://localhost:8000')
    .wait(2000)
    .takeScreenshot()
    .expect(Selector('a').withText('Learn React').exists).eql(true);
})