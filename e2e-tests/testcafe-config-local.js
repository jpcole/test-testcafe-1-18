// e2e-tests/testcafe-config-local.js

const createTestCafe = require('testcafe');
async function configureTestcafe() {
  const testcafe = await createTestCafe('localhost', '9003', '9004');

  try {
    const runner = testcafe.createRunner();

    const failedCount = await runner
      .startApp('node server.js', 15000)
      .src('e2e-tests/tests/')
      .browsers('chrome --allow-insecure-localhost')
//      .concurrency(3)
      .reporter(['spec', {
        name: 'spec',
        output: 'e2e-tests/tests/test-results.txt'
      }])
      .run();

    console.log('Tests failed: ' + failedCount);
    if (failedCount > 0) {
      throw new Error('failed tests so fail job');
    }
  } finally {
    await testcafe.close();
  }
}
configureTestcafe();