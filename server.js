const express = require('express')
const app = express()
const port = 8000
const appRoot = require('app-root-path');
const path = require('path');

app.use(express.static(path.join(appRoot.path, 'build')));

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})